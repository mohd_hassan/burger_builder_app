import React from 'react';
import {connect} from 'react-redux';

import Aux from '../Aux';
import classes from './layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/Sidedrawer/SideDrawer';

class Layout extends React.Component {

  state = {
    showSideDrawer: false
  }

  SideDrawerHandler = () => {
    this.setState( prevState => ({
      showSideDrawer: !prevState.showSideDrawer
    }));
  }

  render() {
    return (
      <Aux>
        <Toolbar isAuth={this.props.isAuthenticated} clicked={this.SideDrawerHandler}/>
        <SideDrawer isAuth={this.props.isAuthenticated} open={this.state.showSideDrawer} sideToggle={this.SideDrawerHandler} />
        <main className={classes.Content}>
          {this.props.children}
        </main>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token != null 
  }
}

export default connect(mapStateToProps)(Layout);