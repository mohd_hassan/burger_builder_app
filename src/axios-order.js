import Axios from "axios";

const instance = Axios.create({
  baseURL: 'https://react-burger-project-a592b.firebaseio.com/'
});

export default instance;