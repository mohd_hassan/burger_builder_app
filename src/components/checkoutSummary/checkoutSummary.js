import React from 'react';

import Burger from '../Burger/Burger';
import Button from '../UI/Button/Button';
import classes from './checkoutSummary.css'

const checkoutSummary = props => {
  return (
    <div className={classes.checkoutSummary}>
      <h1>Your Burger is ready for checkout</h1>
      <div style={{ width: '100%', margin: 'auto' }}>
        <Burger ingredients={props.ingredients}/>
      </div>
      <Button 
      btnType='Danger'
      clicked={props.Canceled} >Cancel</Button>
      <Button 
      btnType='Success'
      clicked={props.Continued} >Continue</Button>
    </div>
  );
}

export default checkoutSummary;