import React from 'react';

import BuildControl from './BuildControl/BuildControl'
import classes from './BuildControls.css';

const buildControls = (props) => {

  const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
  ];

  return (
    <div className={classes.BuildControls} >
      <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
      {controls.map(data => (
        <BuildControl
          key={data.label}
          label={data.label}
          Add={() => props.addIngredient(data.type)}
          Remove={() => props.removeIngredient(data.type)}
          disabled={props.disabled[data.type]} />
      ))}
      <button 
      className={classes.OrderButton} 
      disabled={props.purchasable}
      onClick={props.ordered} >{props.isAuth ? 'Order Now': 'SignIn to Continue'}</button>
    </div>
  );
};

export default buildControls;