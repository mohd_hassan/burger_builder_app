import React from 'react';

import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredients/BurgerIngredients';

const burger = (props) => {

  let transformedIngredients = Object.keys(props.ingredients)
    .map(igName => {
      return [...Array(props.ingredients[igName])].map((_, index) => {
        return <BurgerIngredient key={igName + index} type={igName} />
      })
    }).reduce((prev, current) => {
      return prev.concat(current)
    }, []);

  if(transformedIngredients.length === 0){
    transformedIngredients = <p>Please start adding ingredients!</p>
  }

  return (
    <div className={classes.Burger}>
      <BurgerIngredient type='bread-top' />
      {transformedIngredients}
      <BurgerIngredient type='bread-bottom' />
    </div>
  );
}
export default burger;