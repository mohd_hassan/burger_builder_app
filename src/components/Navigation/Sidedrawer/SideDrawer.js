import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import BackDrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/Aux';

const sideDrawer = (props) => {

  let attachedClasses = [classes.sidedrawer, classes.Closed];
  if (props.open) {
    attachedClasses = [classes.sidedrawer, classes.Open];
  }

  return (
    <Aux>
      <BackDrop show={props.open} clicked={props.sideToggle} />
      <div className={attachedClasses.join(' ')} onClick={props.sideToggle}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <nav>
          <NavigationItems isAuthenticated={props.isAuth} />
        </nav>
      </div>
    </Aux>
  );
};

export default sideDrawer;