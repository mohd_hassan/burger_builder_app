import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import classes from './Auth.css';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import { checkValidation } from "../../utility";

class Auth extends React.Component {

  state = {
    controls: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your Email'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'password',
          placeholder: 'Your password'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false
      }
    },
    isSignUp: false
  }

  componentDidMount() {
    if( !this.props.building && this.props.setAuthRedirectPath !== '/'){
      this.props.onSetAuthRedirectPath();
    }
  }



  inputChangedHandler = (event, name) => {
    const updatedControls = {
      ...this.state.controls,
      [name]: {
        ...this.state.controls[name],
        value: event.target.value,
        valid: checkValidation(event.target.value, this.state.controls[name].validation),
        touched: true
      }
    };
    this.setState({ controls: updatedControls })
  }

  switchModeHandler = () => {
    this.setState((prevState) => {
      return { isSignUp: !prevState.isSignUp };
    });
  }

  submitHandler = (event) => {
    event.preventDefault();
    this.props.onAuth(this.state.controls.email.value, this.state.controls.password.value, this.state.isSignUp);
  }


  render() {

    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      });
    }

    let form =
      formElementsArray.map(formElement => (
        <Input
          key={formElement.id}
          elementType={formElement.config.elementType}
          elementConfig={formElement.config.elementConfig}
          invalid={!formElement.config.valid}
          value={formElement.config.value}
          touched={formElement.config.touched}
          shouldValidate={formElement.config.validation.required}
          changed={(event) => this.inputChangedHandler(event, formElement.id)} />
      ));

    if (this.props.loading)
      form = <Spinner />

    let errorMsg = null;
    if (this.props.error)
      errorMsg = <p style={{ color: 'tomato', fontWeight: 'bold' }}>{this.props.error.message}</p>;

    let authRedirect = null;
    if (this.props.isAuthenticated)
      authRedirect = <Redirect to={this.props.authRedirectPath} />

    return (
      <div className={classes.Auth} >
        {authRedirect}
        <form onSubmit={this.submitHandler} >
          {form}
          <Button btnType='Success'>Submit</Button>
        </form>
        <Button btnType='Danger' clicked={this.switchModeHandler}>{!this.state.isSignUp ? 'Dont have an account? SignUp' : 'Already have an accout? SignIn'}</Button>
        {errorMsg}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    building: state.burgerBuilder.building,
    authRedirectPath: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, pw, isSignUp) => dispatch(actions.auth(email, pw, isSignUp)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);