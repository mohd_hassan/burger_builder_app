import React from 'react';

import CheckoutSummary from '../../components/checkoutSummary/checkoutSummary';
import Contact from './ContactData/contactData';
import { Route, Redirect } from 'react-router-dom';
import { connect } from "react-redux";

class Checkout extends React.Component {

  purchaseCancelHandler = () => {
    this.props.history.goBack();
  }

  purchaseContinueHandler = () => {
    this.props.history.replace('/checkout/contact-data');
  }

  render() {
    let summary = <Redirect to='/' />;
    if (this.props.ings) {
      const purchaseRedirect = this.props.purchased ? <Redirect to='/' /> : null;
      summary = (
        <div>
          {purchaseRedirect}
          <CheckoutSummary
            ingredients={this.props.ings}
            Canceled={this.purchaseCancelHandler}
            Continued={this.purchaseContinueHandler} />
          <Route path={this.props.match.path + '/contact-data'} component={Contact} />
        </div>
      );
    }
    return summary;
  }
}

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    purchased: state.order.purchased
  };
};

export default connect(mapStateToProps)(Checkout);