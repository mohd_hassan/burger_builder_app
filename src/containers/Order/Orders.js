import React from 'react';

import Order from './Order/Order';
import axios from '../../axios-order';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';

class Orders extends React.Component {

  componentDidMount() {
    this.props.onFetchOrders(this.props.token, this.props.userId);
  }

  render() {
    let order = <Spinner />;
    if (!this.props.loading) {
      order = this.props.order.map(data => {
        return <Order
          key={data.id}
          ingredients={data.ingredients}
          price={data.price} />
      })
    }
    return (
      <div>
        {order}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    order: state.order.order,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchOrders: (token, userId) => dispatch(actions.fetchOrder(token, userId))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axios)); 